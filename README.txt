This module extends the Varnish module to purge caches of multiple
configured base urls.

## Installation
You need to have the Varnish module installed and enabled.

Once you have Varnish working, to use the module you need to do the following:

Enable the module.
Configure the following variables in settings.php file:

$conf['base_url_varnish_all_all'] = 'http://example1.co.uk
http://example2.co.uk';
$conf['varnish_ip'] = '127.0.0.1:6082 127.0.0.1:6082';

To use the module go to admin/config/development/performance

Beta -> 7.x-1.x
Dev -> 7.x-2.x (unstable) (broken)
